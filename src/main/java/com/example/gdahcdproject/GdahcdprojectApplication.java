package com.example.gdahcdproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GdahcdprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(GdahcdprojectApplication.class, args);
    }

}
